﻿using TMPro;
using UnityEngine;

public class ResourceUIContent : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI woodText;
    [SerializeField] private TextMeshProUGUI foodText;
    [SerializeField] private TextMeshProUGUI goldText;
    
    [SerializeField] private TextMeshProUGUI villagerOnWoodText;
    [SerializeField] private TextMeshProUGUI villagerOnFoodText;
    [SerializeField] private TextMeshProUGUI villagerOnGoldText;

    public TextMeshProUGUI VillagerOnWoodText
    {
        get => villagerOnWoodText;
        set => villagerOnWoodText = value;
    }

    public TextMeshProUGUI VillagerOnFoodText
    {
        get => villagerOnFoodText;
        set => villagerOnFoodText = value;
    }

    public TextMeshProUGUI VillagerOnGoldText
    {
        get => villagerOnGoldText;
        set => villagerOnGoldText = value;
    }

    [SerializeField] private float updateRate;
    [SerializeField] private float foodWorkRate;
    [SerializeField] private float woodWorkRate;
    [SerializeField] private float goldWorkRate;

    public TextMeshProUGUI WoodText
    {
        get => woodText;
        set => woodText = value;
    }

    public TextMeshProUGUI FoodText
    {
        get => foodText;
        set => foodText = value;
    }

    public TextMeshProUGUI GoldText
    {
        get => goldText;
        set => goldText = value;
    }

    public float UpdateRate
    {
        get => updateRate;
        set => updateRate = value;
    }

    public float FoodWorkRate
    {
        get => foodWorkRate;
        set => foodWorkRate = value;
    }

    public float WoodWorkRate
    {
        get => woodWorkRate;
        set => woodWorkRate = value;
    }

    public float GoldWorkRate
    {
        get => goldWorkRate;
        set => goldWorkRate = value;
    }
}
