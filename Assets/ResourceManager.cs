﻿using System;
using TMPro;
using UniRx;
using UnityEngine;

public enum Resource
{
    Food,
    Wood,
    Gold
}
public class ResourceManager : MonoBehaviour
{
    [SerializeField] private ResourceUIContent uiContent; 
    
    private int woodAmount;
    private int foodAmount;
    private int goldAmount;
    
    private int villagerOnFood;
    private int villagerOnWood;
    private int villagerOnGold;
    void Start()
    {
        Initialize();
        Observable.Timer(TimeSpan.FromSeconds(uiContent.UpdateRate)).Repeat().Subscribe(delegate(long l)
            {
                woodAmount += (int) (villagerOnWood * uiContent.WoodWorkRate);
                foodAmount += (int) (villagerOnFood * uiContent.FoodWorkRate);
                goldAmount += (int) (villagerOnGold * uiContent.GoldWorkRate);
                UpdateResourceTexts();
            });
    }

    private void UpdateResourceTexts()
    {
        uiContent.WoodText.text = woodAmount.ToString();
        uiContent.FoodText.text = foodAmount.ToString();
        uiContent.GoldText.text = goldAmount.ToString();
    }

    private void UpdateVillagerTexts()
    {
        uiContent.VillagerOnFoodText.text = villagerOnFood.ToString();
        uiContent.VillagerOnWoodText.text = villagerOnWood.ToString();
        uiContent.VillagerOnGoldText.text = villagerOnGold.ToString();
    }
    private void Initialize()
    {
        woodAmount = 100;
        foodAmount = 200;
        goldAmount = 50;
        UpdateResourceTexts();
    }

    public void AddVillager(string resource)
    {
        switch (resource)
        {
            case "Food":
                villagerOnFood++;
                break;
            case "Wood":
                villagerOnWood++;
                break;
            case "Gold":
                villagerOnGold++;
                break;
        }
        UpdateVillagerTexts();
    }

}
