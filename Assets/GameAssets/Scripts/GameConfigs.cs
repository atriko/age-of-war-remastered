﻿using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfigs", menuName = "Game/Configs")]
public class GameConfigs : ScriptableObject
{
    [Title("Movement Controller")]
    [SerializeField] private float movementDampening = 1f;
    [SerializeField] private float movementSpeed = 5f;
    [SerializeField] private Vector2 horizontalMovementArea = new Vector2(-2.5f, 2.5f);
    [SerializeField] private Vector2 verticalMovementArea = new Vector2(-2.5f, 2.5f);
    
    public float MovementDampening => movementDampening;
    public float MovementSpeed => movementSpeed;
    public Vector2 HorizontalMovementArea => horizontalMovementArea;
    public Vector2 VerticalMovementArea => verticalMovementArea;
}