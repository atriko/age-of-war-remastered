﻿using System;
using Lean.Touch;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace GameAssets.Scripts
{
    public class SampleMovementController : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private GameConfigs settings;
        [SerializeField] private LeanFingerFilter use = new LeanFingerFilter(true);

        private Camera camera;
        private Vector3 remainingTranslation;
        private bool isTouching = false;
        private IDisposable updateDisposable;
        private Vector3 startPosition = Vector3.zero;

        private void Start()
        {
            camera = Camera.main;
            startPosition = target.transform.position;

            StartMovement();
        }

        private void Reset() => use.UpdateRequiredSelectable(gameObject);
        private void Awake() => use.UpdateRequiredSelectable(gameObject);

        [Button(ButtonSizes.Large), GUIColor(0f, 1f, 0f)]
        public void StartMovement()
        {
            updateDisposable?.Dispose();
            updateDisposable = Observable.EveryUpdate().Where(_ => target).Subscribe(OnUpdate).AddTo(gameObject);
        }

        [Button(ButtonSizes.Large), GUIColor(1f, 0f, 0f)]
        public void StopMovement()
        {
            updateDisposable?.Dispose();
            ResetPosition();
        }

        [Button(ButtonSizes.Large), GUIColor(1f, 1f, 0f)]
        public void ResetPosition()
        {
            target.transform.position = startPosition;
            remainingTranslation = Vector3.zero;
        }

        private void OnEnable()
        {
            LeanTouch.OnFingerDown += OnOnFingerDown;
            LeanTouch.OnFingerUp += OnFingerUp;
        }

        private void OnDisable()
        {
            LeanTouch.OnFingerDown -= OnOnFingerDown;
            LeanTouch.OnFingerUp -= OnFingerUp;
        }

        private void OnFingerUp(LeanFinger obj) => isTouching = false;
        private void OnOnFingerDown(LeanFinger obj) => isTouching = true;

        private void OnUpdate(long obj)
        {
            var oldPosition = target.localPosition;

            if (isTouching)
            {
                var fingers = use.GetFingers();
                var screenDelta = LeanGesture.GetScreenDelta(fingers);
                if (screenDelta != Vector2.zero) Translate(screenDelta);
            }

            remainingTranslation += target.localPosition - oldPosition;
            var factor = LeanTouch.GetDampenFactor(settings.MovementDampening * settings.MovementSpeed, Time.deltaTime);
            var newRemainingTranslation = Vector3.Lerp(remainingTranslation, Vector3.zero, factor);
            Vector3 position = oldPosition + remainingTranslation - newRemainingTranslation;
            position.x = Mathf.Clamp(position.x, settings.VerticalMovementArea.x, settings.VerticalMovementArea.y);
            position.y = target.position.y;
            position.z = Mathf.Clamp(position.z, settings.HorizontalMovementArea.x, settings.HorizontalMovementArea.y);

            target.localPosition = position;
            remainingTranslation = newRemainingTranslation;
        }

        private void Translate(Vector2 screenDelta)
        {
            var screenPoint = camera.WorldToScreenPoint(target.position);
            screenPoint += (Vector3) screenDelta;
            Vector3 position = camera.ScreenToWorldPoint(screenPoint);
            position.x = Mathf.Clamp(position.x, settings.VerticalMovementArea.x, settings.VerticalMovementArea.y);
            position.y = target.position.y;
            position.z = Mathf.Clamp(position.z, settings.HorizontalMovementArea.x, settings.HorizontalMovementArea.y);
            target.position = position;
        }
    }
}