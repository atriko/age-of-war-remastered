﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI
{
    public class TutorialPanel : MonoBehaviour
    {
        [SerializeField] private Button skipButton;
        [SerializeField] private float skipButtonShowDelay = 1;
    
        private void OnEnable()
        {
            skipButton.gameObject.SetActive(false);
            
            
            Observable.Timer(TimeSpan.FromSeconds(skipButtonShowDelay)).Subscribe(delegate(long l)
            {
                skipButton.gameObject.SetActive(true);
            });
        }
    }
}