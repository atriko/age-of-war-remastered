﻿using GameAssets.Scripts.Level;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGamePanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private GameLevelSystem gameLevelSystem;

    private void Start()
    {
        SetLevelText();
    }

    private void SetLevelText()
    {
        levelText.text = $"Level {gameLevelSystem.VirtualLevelIndexForUI + 1}";
    }
}