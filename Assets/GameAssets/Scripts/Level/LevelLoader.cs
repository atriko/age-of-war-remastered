﻿using DHGame.LevelSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameAssets.Scripts.Level
{
    [CreateAssetMenu(menuName = "Framework/Utilities/LevelLoader", fileName = "LevelLoader", order = 0)]
    public class LevelLoader : ScriptableObject
    {
        [SerializeField] private LevelSystem levelSystem;

        public void Load()
        {
            SceneManager.LoadScene("Game");
            SceneManager.UnloadSceneAsync(levelSystem.CurrentLevelIndex);
            SceneManager.LoadScene($"Level{levelSystem.CurrentLevelIndex + 1}", LoadSceneMode.Additive);
        }

        public void LoadFirstLevel()
        {
            if (!SceneManager.GetSceneByName($"Level{levelSystem.CurrentLevelIndex + 1}").isLoaded)
                SceneManager.LoadScene($"Level{levelSystem.CurrentLevelIndex + 1}", LoadSceneMode.Additive);
        }

        public void RestartLevel()
        {
            SceneManager.LoadScene("Game");
            SceneManager.UnloadSceneAsync(levelSystem.CurrentLevelIndex + 1);
            SceneManager.LoadScene($"Level{levelSystem.CurrentLevelIndex + 1}", LoadSceneMode.Additive);
        }
        public void LoadNextLevel()
        {
            levelSystem.ProceedToNextLevel();
            SceneManager.LoadScene("Game");
            SceneManager.UnloadSceneAsync($"Level{levelSystem.CurrentLevelIndex}");
            SceneManager.LoadScene($"Level{levelSystem.CurrentLevelIndex + 1}", LoadSceneMode.Additive);
        }
        public void LoadPreviousLevel()
        {
            levelSystem.ProceedToPreviousLevel();
            SceneManager.LoadScene("Game");
            SceneManager.UnloadSceneAsync($"Level{levelSystem.CurrentLevelIndex + 2}");
            SceneManager.LoadScene($"Level{levelSystem.CurrentLevelIndex + 1}", LoadSceneMode.Additive);
        }
        
    }
}