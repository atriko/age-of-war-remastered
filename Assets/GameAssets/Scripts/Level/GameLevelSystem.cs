using DHGame.LevelSystem;
using UnityEngine;

namespace GameAssets.Scripts.Level
{
    [CreateAssetMenu(menuName = "Framework/Game/GameLevelSystem", fileName = "GameLevelSystem", order = 0)]
    public class GameLevelSystem : LevelSystem
    {
    }
}