﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public class SampleProjectExporter : MonoBehaviour
{
    //[MenuItem("Sample Project Export/Export Now")]
    static void Export()
    {
        string[] assetPaths = {
            "ProjectSettings/EditorBuildSettings.asset",
            "ProjectSettings/GraphicsSettings.asset",
            "ProjectSettings/InputManager.asset",
            "ProjectSettings/ProjectSettings.asset",
            "ProjectSettings/QualitySettings.asset",
            "ProjectSettings/TagManager.asset",
            "ProjectSettings/TimeManager.asset",
            "Assets/ForwardRenderer.asset",
            "Assets/GameAssets",
            "Assets/MMFeedbacks",
            "Assets/Lean",
            "Assets/ProceduralUIImage",
            "Assets/Polygon Arsenal"
        };
        
        string packageName = "SampleProject.unitypackage";
        AssetDatabase.ExportPackage(assetPaths, packageName, ExportPackageOptions.Interactive | ExportPackageOptions.Recurse | ExportPackageOptions.IncludeDependencies);
    }
    
    //[MenuItem("Sample Project Export/Update Settings")]
    static void UpdateSettings()
    {
        GraphicsSettings.renderPipelineAsset = AssetDatabase.LoadAssetAtPath<UnityEngine.Rendering.Universal.UniversalRenderPipelineAsset>("Assets/GameAssets/Settings/LWRP-HighQuality.asset");
        PlayerSettings.companyName = "DreamHarvesters";
    }
}
